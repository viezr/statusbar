'''
Detached statusbar for Linux.
'''
import os
from app import StatusBar
from app.interface import StatusInterface
from app.mpv_player import MpvPlayer


if __name__ == "__main__":
    player = MpvPlayer(socket_file=os.path.expanduser("~/.config/mpv/socket"))
    if not player.ready:
        player = None
    interface = StatusInterface(player=player)
    gui = StatusBar(interface)
    gui.set_geometry()
    gui.mainloop()
