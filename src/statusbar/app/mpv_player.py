'''
Mpv player module.
'''
import os
import json
import socket
from stat import S_ISSOCK

from .subproc_tools import run_cmd


class MpvPlayer():
    '''
    Mpv player class.
    '''
    def __init__(self, socket_file: str = None):
        self.socket = socket_file
        self._ready = False
        self._setup()

    @property
    def ready(self) -> bool:
        return self._ready

    def _setup(self) -> None:
        '''
        Check if player ready for external control.
        '''
        if not os.path.exists(self.socket):
            print(f"Mpv socket file '{self.socket}' not found.")
            return
        mpv_sock_mode = os.stat(self.socket).st_mode
        if not S_ISSOCK(mpv_sock_mode):
            print(f"Not a mpv socket file '{self.socket}'.")
            return
        args = ["ps", "--no-headers", "-o", "%a", "-C", "mpv"]
        data = run_cmd(args, print_err=False)
        if not data:
            print("Mpv is not running.")
            return
        if "--input-ipc-server" not in data:
            print("Mpv running without '--input-ipc-server'")
            return
        try:
            pid = self._control_data('["get_property_string", "pid"]')
        except ConnectionRefusedError:
            print("ERROR. Refused connection to MPV socket.")
            return
        self._ready = True

    def pause(self) -> None:
        '''
        Play-pause control.
        '''
        self._control("cycle pause")

    def prev(self) -> None:
        '''
        Play previous isntance in playlist.
        '''
        self._control("playlist-next")

    def next(self) -> None:
        '''
        Play next isntance in playlist.
        '''
        self._control("playlist-next")

    def del_and_next(self) -> None:
        '''
        Delete current playing file and switch to next in playlist.
        '''
        cwd = self._control_data('["get_property_string", "working-directory"]')
        file = self._control_data('["get_property_string", "path"]')
        file_path = os.path.join(cwd, file)
        self._control("playlist-remove current")
        if os.path.exists(file_path):
            os.remove(file_path)
            print("File deleted:", file_path)
        self._control("playlist-next")

    def title(self) -> str:
        '''
        Get title for current playing instance.
        '''
        title = "unknown"
        data_dict = self._control_data('["get_property", "filtered-metadata"]')
        if not data_dict:
            file_name = self._control_data('["get_property_string", "filename"]')
            file_name = file_name.rsplit(".", 1)[0].replace("_", " ")
            return file_name or title
        if data_dict:
            title = f'{data_dict["Artist"]} - {data_dict["Title"]}'
        return title

    def progress(self) -> int:
        '''
        Get playing progress percent.
        '''
        file_size = self._control_data('["get_property", "file-size"]')
        stream_pos = self._control_data('["get_property", "stream-pos"]')
        percent = int(int(stream_pos) / int(file_size) * 100)
        return percent

    def _control(self, command: str) -> None:
        '''
        Send commands to the player socket.
        '''
        with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as client:
            client.connect(self.socket)
            client.send(bytes(f"{command}\n", "utf-8"))
            client.close()

    def _control_data(self, command: str) -> dict:
        '''
        Send commands to the player socket and return response.
        '''
        data = {}
        with socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) as client:
            client.connect(self.socket)
            client.send(bytes('{"command": ' f'{command}' '}\n', "utf-8"))
            data = client.recv(1024)
            client.close()
        if data:
            data = json.loads(data.decode("utf-8"))["data"]
        return data
