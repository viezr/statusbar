'''
StatusBar main frame.
'''
from datetime import datetime

import tkinter as tk
from tkinter import ttk


class MainFrame(tk.Frame):
    '''
    StatusBar main frame class.
    '''
    def __init__(self, root, container):
        self.root, self.container = root, container
        tk.Frame.__init__(self, self.container)
        self._colors = self.root.style.colors
        self.configure(bg=self._colors["bg"])

        self._fill()

    def _fill(self) -> None:
        '''
        Load widgets.
        '''
        self.columnconfigure(1, weight=1)

        tags_frame = tk.Frame(self, bg=self._colors["bg"])
        tags_frame.grid(row=0, column=0, padx=(0, 20), sticky="W")

        cur_tag, tags_list = self.root.iface.workspaces()
        for idx, tag in enumerate(tags_list):
            tags_label = ttk.Label(tags_frame, text=tag, style="Info.TLabel"
                if idx == cur_tag else "TLabel")
            tags_label.grid(row=0, column=idx, padx=4, sticky="W")

        self.extra_frame = tk.Frame(self, bg=self._colors["bg"])
        self.extra_frame.grid(row=0, column=1, sticky="WE")
        self.extra_frame.columnconfigure(0, weight=1)

        status_frame = tk.Frame(self, bg=self._colors["bg"])
        status_frame.grid(row=0, column=2, padx=(20, 0), sticky="E")

        battery_val = self.root.iface.battery()
        if battery_val:
            battery_label = ttk.Label(status_frame, text=battery_val,
                image=self.root.static["battery"], compound="left")
            battery_label.grid(row=0, column=0, sticky="E")

        self.vol_label = ttk.Label(status_frame, text="",
            image=self.root.static["volume"], compound="left")
        self.vol_label.grid(row=0, column=1, padx=4, sticky="E")

        self.kbd_label = ttk.Label(status_frame, text="",
            style="Title.TLabel")
        self.kbd_label.grid(row=0, column=2, padx=(10, 4), sticky="E")

        self.date_label = ttk.Label(status_frame, text="")
        self.date_label.grid(row=0, column=3, padx=4,
            sticky="E")

    def update(self) -> None:
        '''
        Update date and time label.
        '''
        kbd_val = self.root.iface.kbd_layout()
        self.kbd_label.configure(text=kbd_val)
        vol_val = self.root.iface.volume()
        self.vol_label.configure(text=vol_val)
        date = datetime.now().strftime("%b %d  %a  %H:%M")
        self.date_label.configure(text=date)
        self._update_extra()

    def _update_extra(self) -> None:
        extra_frame_children = self.extra_frame.winfo_children()
        for i in extra_frame_children:
            i.destroy()
        next_col = 1
        extra_info_list = self.root.iface.extra()
        if extra_info_list:
            for state in extra_info_list:
                extra_label = ttk.Label(self.extra_frame, text=state,
                    style="Info.TLabel")
                extra_label.grid(row=0, column=next_col, padx=4)
                next_col += 1
