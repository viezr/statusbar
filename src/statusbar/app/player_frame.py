'''
StatusBar player frame.
'''
import tkinter as tk
from tkinter import ttk


class PlayerFrame(tk.Frame):
    '''
    StatusBar player frame class.
    '''
    def __init__(self, root, container):
        self.root, self.container = root, container
        tk.Frame.__init__(self, self.container)
        self._colors = self.root.style.colors
        self.configure(bg=self._colors["bg"])

        self._fill()

    def _fill(self) -> None:
        '''
        Load widgets.
        '''
        self.columnconfigure(0, weight=1)

        self.title_label = ttk.Label(self, text="", width=60)
        self.title_label.grid(row=0, column=0, padx=(4, 10), sticky="WE")

        control_frame = tk.Frame(self, bg=self._colors["bg"])
        control_frame.grid(row=0, column=1, sticky="E")
        prev_btn = ttk.Button(control_frame, image=self.root.static["prev"],
            command=self.prev)
        prev_btn.grid(row=0, column=2, padx=0, sticky="E")
        pause_btn = ttk.Button(control_frame, image=self.root.static["pause"],
            command=self.pause, width=3)
        pause_btn.grid(row=0, column=3, padx=4, sticky="E")
        next_btn = ttk.Button(control_frame, image=self.root.static["next"],
            command=self.next)
        next_btn.grid(row=0, column=4, padx=(0, 4), sticky="E")

        bar_frame = tk.Frame(self, bg=self._colors["bg"])
        bar_frame.grid(row=1, column=0, columnspan=2, pady=(4, 0), sticky="WE")
        bar_frame.columnconfigure(0, weight=1)
        self.progress_bar = ttk.Progressbar(bar_frame, orient='horizontal',
            mode='determinate', style="Horizontal.TProgressbar")
        self.progress_bar.grid(row=0, column=0, padx=4, stick="WE")

    def prev(self, _event = None) -> None:
        '''
        Play previous track.
        '''
        self.root.iface.player_prev()
        self.after(200, self.update)

    def next(self, _event = None) -> None:
        '''
        Play next track.
        '''
        self.root.iface.player_next()
        self.after(200, self.update)

    def del_and_next(self, _event = None) -> None:
        '''
        Delete current track file and play next track.
        '''
        self.root.iface.player_del_and_next()
        self.after(200, self.update)

    def pause(self, _event = None) -> None:
        '''
        Play/pause current track.
        '''
        self.root.iface.player_pause()

    def update(self) -> None:
        '''
        Update title and progress bar.
        '''
        title = self.root.iface.player_title()
        self.title_label.configure(text=title)
        percent = self.root.iface.player_progress()
        self.progress_bar["value"] = percent
