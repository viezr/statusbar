'''
Subprocess tools module.
'''
import os
from subprocess import run, CalledProcessError

from .config import Config


def ignore_failed(func: callable) -> callable:
    '''
    Decorator to skip commands to run for early failed runs.
    '''
    ignored_args = []
    def wrapper(run_args: list, **kwargs) -> str:
        data = ""
        if run_args in ignored_args:
            return data
        data = func(run_args)
        if not data:
            ignored_args.append(run_args)
        return data
    return wrapper

@ignore_failed
def run_cmd(run_args: list, print_err: bool = True) -> str:
    '''
    Run system command for gathering information.
    '''
    data = stderr = ""
    try:
        data = run(run_args, capture_output=True, check=True)
        if not data:
            stderr = data.stderr.decode("utf-8").strip()
        data = data.stdout.decode("utf-8").strip()
    except (FileNotFoundError, CalledProcessError) as err:
        if print_err:
            print(f"Run failed: {err}")
    else:
        if stderr and print_err:
            print(f"Run failed: {stderr}")
    return data

def read_file(file: str) -> str:
    '''
    Read local file.
    '''
    data = ""
    try:
        with open(file, encoding="utf-8") as fdesc:
            data = fdesc.read()
    except OSError as err:
        print(f"Read file '{file}' error: {err}")
    return data

def workspaces_xprop() -> list:
    '''
    Get workspaces names from 'X' properties.
    '''
    data = run_cmd(["xprop", "-root", "_NET_DESKTOP_NAMES"])
    ws_list = []
    if not data:
        return ws_list
    try:
        data = data.split(" = ", 1)[1]
        ws_list = list(x.strip().replace('"', '') for x in data.split(","))
    except (TypeError, ValueError, IndexError) as err:
        print(f"Workspaces names parsing error: {err}")
    return ws_list

def workspaces_current_xprop() -> int:
    '''
    Get current workspace number from 'X' properties.
    '''
    data = run_cmd(["xprop", "-root", "_NET_CURRENT_DESKTOP"])
    current = 0
    if not data:
        return current
    try:
        current = int(data.split(" = ", 1)[1])
    except (TypeError, ValueError, IndexError) as err:
        print(f"Current workspace number error: {err}")
    return current

def battery_power_supply() -> str:
    '''
    Read battery status from '/sys/class/power_supply'.
    '''
    return read_file("/sys/class/power_supply/BAT0/capacity")

def kbd_layout_xset() -> int:
    '''
    Get current keyboard layouts from 'xset'.
    '''
    data = run_cmd(["xset", "-q"])
    if not data:
        return -1
    layout_num = int(data.split("LED mask:")[1].split("\n")[0].strip())
    return layout_num

def kbd_layouts_setxkbmap() -> list:
    '''
    Get keyboard layouts from 'sexkbmap'.
    '''
    layouts = []
    data = run_cmd(["setxkbmap", "-print", "-verbose", "10"])
    if not data:
        return layouts
    layouts_raw = [x for x in data.split("\n") if "layout:" in x.lower()][0]
    layouts = layouts_raw.split(":")[1].strip().split(",")
    return layouts

def volume_amixer() -> int:
    '''
    Get volume level from 'amixer'.
    '''
    data = run_cmd(["amixer", "-c", "0", "sget", "Master"])
    if not data:
        data = run_cmd(["amixer", "-c", "1", "sget", "Master"])
    if not data:
        return ""
    vol = int(data.split("[", 1)[1].split("%", 1)[0])
    return vol

def extra_info() -> list:
    '''
    Get list of labels for files content.
    '''
    out_list = []
    data = read_file(Config.EXTRA_INFO_FILE)
    if not data:
        return out_list
    extra_list = [x.strip().split(";", 1) for x in data.split("\n")
        if x.strip() and not x.startswith("#")]
    if not extra_list:
        return out_list
    for file, symbol in extra_list:
        file = os.path.expanduser(file) if file.startswith("~") else file
        data = read_file(file).strip()
        if data and data != "0":
            out_list.append(symbol)
    return out_list
