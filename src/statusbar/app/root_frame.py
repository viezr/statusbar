'''
StatusBar main GUI module.
'''
import os
import sys
import tkinter as tk
from PIL import Image, ImageTk

from .config import Config, Style
from .main_frame import MainFrame
from .player_frame import PlayerFrame
from .interface import StatusInterface


class StatusBar(tk.Tk):
    '''
    StatusBar root GUI class.
    '''
    def __init__(self, interface: StatusInterface):
        self._wtitle = Config.APP_TITLE
        tk.Tk.__init__(self, className=self._wtitle)
        self.attributes("-topmost", True)
        self.title(self._wtitle)
        self.style = Style(sys.platform)
        self._colors = self.style.colors
        self.configure(background=self.style.colors["bg"])
        self.columnconfigure(0, weight=1)

        self.iface = interface
        self.static = self._load_static()

        self.main_frame = MainFrame(self, self)
        self.main_frame.grid(row=0, column=0, padx=4, pady=4, sticky="WE")
        self.player_frame = None
        if self.iface.player:
            self.player_frame = PlayerFrame(self, self)
            self.player_frame.grid(row=1, column=0, padx=(0, 4), pady=4,
                sticky="NSWE")
            self.bind("<Control-l>", self.player_frame.del_and_next)
            self.bind("<Key-l>", self.player_frame.next)
            self.bind("<Key-h>", self.player_frame.prev)
            self.bind("<space>", self.player_frame.pause)
        self.bind("<Control-q>", self._quit)
        self.bind("<Escape>", self._quit)
        self._update()

    def _load_static(self) -> dict:
        '''
        Load static images.
        '''
        images = {}
        files = ("next.png", "prev.png", "pause.png", "play.png",
            "battery.png", "volume.png")
        for file in files:
            file_name, _ext = file.rsplit(".", 1)
            file_path = os.path.join("app", "static", file)
            with Image.open(file_path) as img:
                images[file_name] = ImageTk.PhotoImage(image=img)
        return images

    def _update(self) -> None:
        '''
        Update date and time label.
        '''
        self.main_frame.update()
        if self.player_frame:
            self.player_frame.update()
        self.after(Config.LIVE_UPDATE_SEC * 1000, self._update)

    def set_geometry(self) -> None:
        '''
        Set size and position of window.
        '''
        self.update_idletasks()
        scr_w, scr_h = self.winfo_screenwidth(), self.winfo_screenheight()
        wnd_w, wnd_h = self.winfo_width(), self.winfo_height()
        self.geometry(''.join([
            str(wnd_w), "x", str(wnd_h), "+",
            str( int((int(scr_w) - wnd_w) / 2) ), "+",
            str( 10 )
        ]))

    def _quit(self, *_event) -> None:
        '''
        Quit application.
        '''
        self.destroy()
