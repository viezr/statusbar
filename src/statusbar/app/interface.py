'''
Status interface module.
'''
from shutil import which

from .config import Config
from .subproc_tools import (workspaces_xprop, workspaces_current_xprop,
    battery_power_supply, volume_amixer, extra_info, kbd_layout_xset,
    kbd_layouts_setxkbmap)


class StatusInterface():
    '''
    Status interface class
    '''
    def __init__(self, player = None):
        self.player = player
        self._layouts = None
        self._setup()

    def _setup(self) -> None:
        '''
        Check necessary system programs and set keyboard layouts.
        '''
        for app in Config.EXT_APPS:
            if not which(app):
                print(f"Program '{app}' not found.")
        layouts_list = kbd_layouts_setxkbmap()
        self._layouts = dict(zip((0, 1000), layouts_list))

    def workspaces(self) -> tuple:
        '''
        Get current workspace and workspaces names.
        '''
        ws_list = workspaces_xprop()
        if not ws_list:
            return (0, ws_list)
        current_num = workspaces_current_xprop()
        return (current_num, ws_list)

    def extra(self) -> list:
        '''
        Get string values to show in status for extra files checking.
        '''
        return extra_info()

    def battery(self) -> str:
        '''
        Get battery charge level.
        '''
        data = battery_power_supply()
        state = f"{data}%" if data else ""
        return state

    def volume(self) -> str:
        '''
        Get volume.
        '''
        data = volume_amixer()
        state = f"{data}%" if data else ""
        return state

    def kbd_layout(self) -> str:
        '''
        Get current keyboard layout.
        '''
        layout_num = kbd_layout_xset()
        state = self._layouts.get(layout_num, "Err").upper()
        return state

    def player_pause(self) -> None:
        '''
        Send 'play-pause' command to the player.
        '''
        self.player.pause()

    def player_prev(self) -> None:
        '''
        Send 'previous' command to the player.
        '''
        self.player.prev()

    def player_next(self) -> None:
        '''
        Send 'next' command to the player.
        '''
        self.player.next()

    def player_del_and_next(self) -> None:
        '''
        Delete current playing file and switch to next in playlist.
        '''
        self.player.del_and_next()

    def player_title(self) -> tuple:
        '''
        Get title from the player.
        '''
        return self.player.title()

    def player_progress(self) -> tuple:
        '''
        Get playing progress percent from the player.
        '''
        return self.player.progress()
