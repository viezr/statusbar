'''
App config
'''


class Config():
    '''
    Config attributes
    '''
    APP_TITLE = "Statusbar"
    LIVE_UPDATE_SEC = 10
    EXTRA_INFO_FILE = "extra.txt"
    EXT_APPS = ["ps", "setxkbmap", "xset", "xprop", "amixer"]
