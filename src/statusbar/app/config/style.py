'''
Styles for appllication
'''
from tkinter import ttk


class Style(ttk.Style):
    '''
    Main class for application style.
    '''
    colors = {
        "fg": "#ffffff",
        "bg": "#272c33",
        "bg_mid": "#2c313a",
        "bg_light": "#384056",
        "select_dark": "#143b52",
        "select": "#2e8bc0",
        "select_light": "#d4f1f4",
        "select_mid": "#b1d4e0",
        "accent_bg_info": "#f28102",
        "accent_bg_good": "#59981a",
        "accent_bg_bad": "#ffaebc",
        "links": "#2299ff"
    }

    def __init__(self, platform: str):
        ttk.Style.__init__(self)

        self._platform = platform
        self.colors=__class__.colors
        self.map(".", background=[("disabled", self.colors["bg_mid"])])

        self.configure(".",
            background=self.colors["bg"],
            foreground=self.colors["fg"],
            troughcolor=self.colors["bg"],
            selectbg=self.colors["select"],
            selectfg=self.colors["fg"],
            fieldbg=self.colors["bg"],
            font=("TkDefaultFont",),
            borderwidth=1,
            bordercolor=self.colors["bg"],
            focuscolor=self.colors["bg"]
        )

        if self._platform == "win32":
            self.configure(".", foreground="#000000")

        self._configure_buttons()
        self._configure_labels()
        self._configure_progressbar()

    def _configure_buttons(self) -> None:
        '''
        Configure buttons style.
        '''
        self.configure("TButton", padding=(8, 3, 8, 3), width=12,
            anchor="center", relief="flat", font=("Sans","10"))
        self.map("TButton",
            background=[
                ("!active", self.colors["bg_light"]),
                ("active", self.colors["select"]),
                ("!focus", self.colors["bg_light"]),
                ("focus", self.colors["select"])
            ]
        )

        if self._platform == "win32":
            self.map("TButton", foreground=[("active", "#000000")])

    def _configure_labels(self) -> None:
        '''
        Configure labels style.
        '''
        self.configure("TLabel", background=self.colors["bg"],
            font=("Sans","10"))
        self.configure("Title.TLabel", font=("Sans","10","bold"))
        self.configure("Small.TLabel", font=("Sans","8"))
        self.configure("SmallGray.TLabel",
            foreground=self.colors["accent_bg_info"], font=("Sans","8"))

        self.map("Info.TLabel",
            foreground=[
                ("!focus", self.colors["accent_bg_info"])
            ],
            background=[
                ("!focus", self.colors["bg"])
            ]
        )

    def _configure_progressbar(self) -> None:
        '''
        Configure progressbar style.
        '''
        self.configure("Horizontal.TProgressbar",
            background=self.colors["select"], thickness=2)
