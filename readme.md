## statusbar
Detached status bar for Linux.  
The purpose for creating this status bar is extending my working area by
removing default status bar and showing information with some additional
features only when I need it.  
![image](screenshot.png)  

## Features
- show workspaces names, current workspace, date, battery and volume levels
- show additional information as preset values for content of defined files
- media player control (for 'mpv' yet, but may be extended with another module)
- media player title and progress bar
- live update information (by default every 10 seconds)

## Shortcuts
- Esc - close app
- H - play previus track
- L - play next track
- Ctrl+L - delete (!not just moving to trash folder) current playing file and
  play next
- Space - play/pause

## Notes
Workspaces names and current workspace gathered from 'X' properties:
'_NET_DESKTOP_NAMES' and '_NET_CURRENT_DESKTOP'.
For 'dwm' I use 'ewmhtags' patch to get such properties.  
Workspaces icons on the screenshot are just a text from '_NET_DESKTOP_NAMES'
with 'Awesome' font and not implemented in the application.

## Additional information for status bar
Files list for extra information loaded from file `extra.txt` with content like:   
```
~/.config/some-state-file1;SMART
~/.config/some-state-file2;SRV_DWN
```
If file contains non-zero value or any other data, status bar will draw text
after ';' separator (e.g. 'shield' symbol on the screenshot).  

## Usage
`python3 sbar.py`
